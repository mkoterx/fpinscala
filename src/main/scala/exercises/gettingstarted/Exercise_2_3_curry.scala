package exercises.gettingstarted

object Exercise_2_3_curry {

  def curry[A, B, C](f: (A, B) => C): A => (B => C) =
    a => b => f(a, b)
}
