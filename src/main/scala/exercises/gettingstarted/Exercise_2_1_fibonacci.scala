package exercises.gettingstarted

object Exercise_2_1_fibonacci {

  def main(args: Array[String]): Unit = {
    println("Fibonacci[%d] = %d".format(0, fib(0)))
    println("Fibonacci[%d] = %d".format(1, fib(1)))
    println("Fibonacci[%d] = %d".format(2, fib(2)))
    println("Fibonacci[%d] = %d".format(3, fib(3)))
    println("Fibonacci[%d] = %d".format(4, fib(4)))
    println("Fibonacci[%d] = %d".format(5, fib(5)))
    println("Fibonacci[%d] = %d".format(6, fib(6)))
  }

  def fib(n: Int): Int = {
    @annotation.tailrec
    def go(n: Int, prev: Int, curr: Int): Int = {
      if (n == 0) prev
      else if (n == 1) curr
      else go(n - 1, curr, prev + curr)
    }
    go(n, 0, 1)
  }
}
