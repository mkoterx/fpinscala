package exercises.gettingstarted

object Exercise_2_4_uncurry {

  def uncurry[A, B, C](f: A => B => C): (A, B) => C =
    (a, b) => f(a)(b)
}
