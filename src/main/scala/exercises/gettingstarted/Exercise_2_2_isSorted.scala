package exercises.gettingstarted

object Exercise_2_2_isSorted {

  def main(args: Array[String]): Unit = {
    val a1 = Array(1, 4, 2, 5, 7, 3)
    println(isSorted[Int](a1, _ < _))
    val a2 = Array(1, 4, 5, 7, 9)
    println(isSorted[Int](a2, _ < _))
    val a3 = Array("Any", "Bob", "Carry")
    println(isSorted[String](a3, (s1, s2) => s1.compareTo(s2) < 0))
  }

  /**
    *
    * @param as array of type A
    * @param st smallerThan function
    * @tparam A
    * @return true if the arrays is sorted according to the order function and false otherwise
    */
  def isSorted[A](as: Array[A], st: (A, A) => Boolean): Boolean = {
    def go(n: Int): Boolean =
      if (n == as.length - 1) true
      else if (!st(as(n), as(n + 1))) false
      else go(n + 1)
    go(0)
  }
}
