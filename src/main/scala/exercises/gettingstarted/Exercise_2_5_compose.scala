package exercises.gettingstarted

object Exercise_2_5_compose {

  def compose[A, B, C](f: B => C, g: A => B): A => C =
    a => f(g(a))
}
