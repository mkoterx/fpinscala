package exercises.datastructures

object Exercise_3_10_foldLeft {

  def foldLeft[A, B](as: List[A], z: B)(f: (A, B) => B): B = as match {
    case Nil => z
    case h::t => foldLeft(t, f(h, z))(f)
  }
}
