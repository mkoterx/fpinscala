package exercises.datastructures

object Exercise_3_9_len_using_foldRight {

  def main(args: Array[String]): Unit = {
    val as = List(1, 4, 2, 5)
    println("Length is %d".format(length(as)))
  }

  def length[A](as: List[A]): Int = {
    import examples.FoldRight.foldRight
    foldRight(as, 0)((_, acc) => acc + 1)
  }
}
