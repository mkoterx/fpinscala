package exercises.datastructures

object Exercise_3_6_init {

  def main(args: Array[String]): Unit = {
    val as = List(1, 4, 2, 5, 1)
    println(init(as))
  }

  def init[A](l: List[A]): List[A] = l match {
    case Nil => sys.error("init of empty list")
    case h::Nil => Nil
    case h::t => h::init(t)
  }
}
