package exercises.datastructures

object Exercise_3_13_fold_using_fold {

  def foldRight[A, B](as: List[A], z: B)(f: (A, B) => B): B = as match {
    case Nil => z
    case h::t => f(h, foldRight(t, z)(f))
  }

  def foldLeft[A, B](as: List[A], z: B)(f: (A, B) => B): B = as match {
    case Nil => z
    case h::t => foldLeft(t, f(h, z))(f)
  }

  def foldLeft2[A, B](as: List[A], z: B)(f: (A, B) => B): B =
    foldRight(as, z)()

}
