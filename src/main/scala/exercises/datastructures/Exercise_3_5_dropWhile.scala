package exercises.datastructures

object Exercise_3_5_dropWhile {

  def main(args: Array[String]): Unit = {
    val ds = List(2, 1, 5, 2, 4, 1)
    println(dropWhile(ds)(_ % 2 == 0))
  }

  // We group into two argument lists (now dropWhile is curried)
  // to improve the type inference when calling dropWhile
  def dropWhile[A](l: List[A])(f: A => Boolean): List[A] = l match {
    case Nil => Nil
    case h::t if f(h) => dropWhile(t)(f)
    case _ => l
  }
}
