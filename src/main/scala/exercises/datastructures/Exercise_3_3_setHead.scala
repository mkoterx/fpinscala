package exercises.datastructures

object Exercise_3_3_setHead {

  def main(args: Array[String]): Unit = {
    val ds = List(2, 4, 5, 2)
    println(setHead(ds, 8))
  }

  def setHead[A](l: List[A], h: A): List[A] = l match {
    case Nil => sys.error("setHead on empty list")
    case _::xs => h::xs
  }
}
