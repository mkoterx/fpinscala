package exercises.datastructures

object Exercise_3_2_tail {

  def main(args: Array[String]): Unit = {
    val ds = List(2, 4, 5, 2)
    println(tail(ds))
  }

  def tail[A](l: List[A]): List[A] = l match {
    case Nil => sys.error("tail of empty list")
    case _::t => t
  }
}
