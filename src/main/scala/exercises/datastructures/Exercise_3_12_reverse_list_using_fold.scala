package exercises.datastructures

import Exercise_3_10_foldLeft.foldLeft

object Exercise_3_12_reverse_list_using_fold {

  def main(args: Array[String]): Unit = {
    val as = List(1, 2, 3)
    println("Reverse of " + as + ": " + reverse(as))
  }

  def reverse[A](ds: List[A]): List[A] = foldLeft(ds, List[A]())(_ :: _)
}
