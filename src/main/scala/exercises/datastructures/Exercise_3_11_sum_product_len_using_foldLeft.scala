package exercises.datastructures

import Exercise_3_10_foldLeft.foldLeft

object Exercise_3_11_sum_product_len_using_foldLeft {

  def sum(ints: List[Int]): Int = foldLeft(ints, 0)(_ + _)

  def product(ds: List[Double]): Double = foldLeft(ds, 1.0)(_ * _)

  def length[A](ds: List[A]): Int = foldLeft(ds, 0)((_, acc) => acc + 1)
}
