package exercises.datastructures

object Exercise_3_4_drop {

  def main(args: Array[String]): Unit = {
    val ds = List(2, 4, 5, 2, 4, 1)
    println(drop(ds, 3))
  }

  def drop[A](l: List[A], n: Int): List[A] =
    if (n <= 0) l
    else l match {
      case Nil => Nil
      case _::xs => drop(xs, n - 1)
    }
}
