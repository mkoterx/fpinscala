package examples

object PartialFunction {

  def partial1p[A, B, C](a: A, f: (A, B) => C): B => C =
    (b: B) => f(a, b)
}
