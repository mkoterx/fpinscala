package examples

object FoldRight {

  def sum(ints: List[Int]): Int = ints match {
    case Nil => 0
    case h::t => h + sum(t)
  }

  def product(ds: List[Double]): Double = ds match {
    case Nil => 1.0
    case h::t => h * product(t)
  }

  /*
  The previous two functions are very similar. They operate on different types (List[Int] versus List[Double]),
  but aside from that, the only differences are the value to return in the case that the list is empty and the
  operation to combine results.
  Lets generalize this. We will write a function that takes as arguments the value to return in the case of
  an empty list, and the function to add an element to the result in the case of a nonempty list.
   */

  // Note: Placing f in its own argument group is only to improve type inference
  def foldRight[A, B](as: List[A], z: B)(f: (A, B) => B): B = as match {
    case Nil => z
    case h::t => f(h, foldRight(t, z)(f))
  }

  def sum2(ns: List[Int]): Int =
    foldRight(ns, 0)(_ + _)

  def product2(ns: List[Double]): Double =
    foldRight(ns, 1.0)(_ * _)

  def main(args: Array[String]): Unit = {
    val as = List(1, 3, 5, 2)
    println("Sum: %d".format(sum2(as)))
    val bs = List[Double](4, 5, 2)
    println("Product: %f".format(product2(bs)))
  }
}
