package examples

class Cafe {
  // Method with side-effect (hard to test)
  def buyCoffee(cc: CreditCard): Coffee = {
    val cup = new Coffee()
    cc.charge(cup.price) // side-effect
    cup
  }

  /*
  Adding a payments object (still has side-effect)

  Payments can be an interface and we can write a mock impl suitable for testing, but still has the following downsides:
  1. We are forced to make Payments and interface, when a concrete class may be fine

  2. The mock impl might contain some internal state that we'll have to inspect after the call to buyCoffee, and our
  test will have to make sure this state has been appropriately modified (mutated) by the call to charge

  3. We can use a mock framework to handle this, but still a lot of overkill just to test if buyCoffee creates a charge
  equal to the price of a cup of coffee

  4. Difficult to reuse buyCoffee. For example ordering 12 cups of coffee would involve contacting the payment system
  12 times, authorizing 12 separate charges thus adding more processing fees.
  */
  def buyCoffeeImproved(cc: CreditCard, p: Payments): Coffee = {
    val cup = new Coffee()
    p.charge(cc, cup.price)
    cup
  }

  /*
  Functional solution: removing side-effects

  Now we return the charge as a value in addition to returning the Coffee.

  Here we separate the concern of creating a charge from the processing or interpretation of that charge.

  The concerns of processing the charge by sending it off to the credit card company, persisting a record of it,
  and so on, will be handled elsewhere.

  Now we are able to reuse buyCoffee directly to define buyCoffees, and both functions are trivially testable
  without having to define complcated mock impl of some Payments interface.
  */
  def buyCoffeeFunctional(cc: CreditCard): (Coffee, Charge) = {
    val cup = new Coffee()
    (cup, Charge(cc, cup.price))
  }

  // implemented in terms of buyCoffeeFunctional
  def buyCoffees(cc: CreditCard, n: Int): (List[Coffee], Charge) = {
    val purchases: List[(Coffee, Charge)] = List.fill(n)(buyCoffeeFunctional(cc))
    val (coffees, charges) = purchases.unzip
    (coffees, charges.reduce((c1, c2) => c1.combine(c2)))
  }
}

class CreditCard(val number: Int) {
  def charge(amount: Double): Unit = println("Charged %f dollars".format(amount))
}

class Coffee(val price: Int = 10)

trait Payments {
  def charge(cc: CreditCard, amount: Double): Unit
}

/*
Making Charge into a first-class value has also the benefit of more easily assembling business logic for working
with these charges. For instance, Alice may bring ger laptop to the coffee shop and work there for a few hours, making
occasional purchases. Im might be nice if the coffee shop could combine these purchases Alice makes into a single charge,
again saving on credit card processing fees. (see the coalesce method)
*/
case class Charge(cc: CreditCard, amount: Double) {
  def combine(other: Charge): Charge = {
    if (cc == other.cc)
      Charge(cc, amount + other.amount)
    else
      throw new Exception("Can't combine charges to different cards")
  }

  def coalesce(charges: List[Charge]): List[Charge] =
    charges.groupBy(_.cc).values.map(_.reduce(_ combine _)).toList
}
