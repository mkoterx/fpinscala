package examples

object PolymorphicFunc {

  def main(args: Array[String]): Unit = {
    val z = Array("Zara", "Nuha", "Ayan")
    println(findFirst[String](z, _.equals("Nuha")))
    println(findFirst[String](z, _.equals("Barber")))
  }

  def findFirst[A](as: Array[A], p: A => Boolean): Int = {
    @annotation.tailrec
    def go(n: Int): Int =
      if (n >= as.length) -1
      else if (p(as(n))) n
      else go(n + 1)
    go(0)
  }
}
